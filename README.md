> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368

## Kristopher Mangahas

### Assignment 5 # Requirements

*Deliverables:*

1. Provide Bitbucket read-only access to a5 repo, *must* include README.md, using Markdown syntax
2. Blackboard Links: a5 Bitbucket repo
4. Screenshot : Success of Program
5. Screenshot : Customer Table Before New Entry
6. Screenshot : Customer Table After New Entry



#### Assignment Screenshots:


*Screenshot of Customer Table Before Entry*:

![Customer Table Screenshot](img/before.png)


*Screenshot of a passed validation*:

![Passed validation Screenshot](img/passed.png)



*Screenshot of Customer Table After Entry*:

![Customer Table Screnshot](img/after.png)